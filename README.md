Uputstvo za fajlove za vođenje evidencije o studentima, verzija 3.2
===================================================================

Dokumenti su testirani sa verzijom *LibreOffice*‑a 5.2.7.2 i nisu
testirani sa 4.X verzijama već dugo vremena (mada najverovatnije rade sa
starijim 5.X verzijama). Verzija 6.0 još nije testirana.

BonusPredavanja.ods
-------------------

Dokument je namenjen za beleženje bonus poena na predavanjima. Kada neki
student prvi put dobije bonus na predavanjima, njegov indeks (u obliku
ra123-2018) treba upisati u kolonu *Indeks*, ime i prezime u narednu
kolonu, a onda u kolonu sa rednim brojem predavanja treba upisati broj
poena koje je student dobio. Zasad su predviđena 32 termina za
predavanja (bazirano na 2 predavanja nedeljno, 30 bi trebalo da je
dovoljno; ako treba više, javite). Podaci iz ovog dokumenta se mogu
direktno uvući u Evidencija.ods (tabela *Alati*, odeljak
*Unos aktivnosti studenata*).

Pregled.ods
-----------

Preptostavka je da će za svaku grupu zadatke pregledati jedan asistent i
da na kraju sve te pregledane grupe treba objediniti u jedan spisak. U
ovom fajlu, tačnije u fajlovima nastalim od njega, se nalaze podaci o
jednoj grupi studenata. Fajl se sastoji od 3 tabele: *Pregledanje*,
*Raspored* i *Statistika*. **Tamno zasenčena polja nisu predviđena da se
ručno menjaju**. Takođe, prazan *Pregled.ods* fajl mora da se nalazi **u
istom direktorijumu** kao i fajl *Evidencija.ods*, u suprotnom
automatsko generisanje popunjenih *Pregled.ods* fajlova neće biti
moguće.

### Tabela Pregledanje

U ovoj tabeli se nalaze podaci o studentima. Tabela je podeljena na dva
glavna dela: u gornjem se nalaze podaci o maksimalnom broju poena za
različita bodovanja testova (do 5 različitih bodovanja), a u donjem delu
se nalaze podaci o osvojenim poenima, za do dva zadatka.

Mogućnost različitog bodovanja istog testa je proistekla iz pravila za
polaganje ispita na Arhitekturi računara i Operativnim sistemima, gde se
na kraju semestra održava nadoknada jednog propuštenog testa (redovni
testovi mogu imati različit maksimalan broj poena). Ukoliko se ova
mogućnost ne koristi, redovi *Poeni 2* do *Poeni 5* se mogu ignorisati
(pri tome, nule u poljima za alternativna bodovanja treba da ostanu).

U donjem delu tabele, podaci o radnom mestu (*Mesto*), broju indeksa
(*Indeks*) i imenu i prezimenu (*Ime i prezime*) se mogu ručno upisati,
a mogu se popuniti i automatski (ovo je preporučeno; bazirano je na
fajlu koji se može izgenerisati na serveru za vreme provere i koji se
nalazi u arhivi sa rezultatima). Za automatsko popunjavanje је zadužen
odeljak *Generisanje Pregled.ods fajla* u tabeli *Alati* u
*Evidencija.ods* fajlu.

Polje *PIsp* sadrži broj poena koje je student imao na prethodnim
predispitnim obavezama. Može poslužiti kao orijentacija za ocenjivanje.

Polje *Nado* služi kao indikacija nadoknade nekog ranijeg testa. Ukoliko
se tu nalazi prazno polje ili crtica, studentu će se računati poeni koji
stoje u redu *Poeni 1* u gornjem delu tabele. Ukoliko treba da se
koristi alternativno bodovanje, u ovo polje se može upisati broj od 1 do
5, čime će se odabrati jedan od 5 alternativnih bodovanja. Ukoliko se
koristi automatsko popunjavanje *Pregled.ods* fajla, u ovu kolonu će se
upisati sadržaj *PravoN* kolone iz *Evidencija.ods*.

Polje *Prepis* služi za zapisivanje informacije o eventualnom
prepisivanju. Ako se u ovo polje upiše bilo kakav tekst (npr. redi broj
mesta sa koga je prepisivano), prilikom ubacivanja rezultata u dokument
*Evidencija.ods* će se u kolonu *Prepis* upisati naziv testa. U polju
*Poeni* nalaze ukupni osvojeni poeni na proveri (uključujući i
eventualnu korekciju rezultata iz tabele *Statistika*).

Odeljci *Zadatak1* i *Zadatak2* služe za ocenjivanje pojedinih zadataka.
Svaki zadatak se može podeliti na do 5 delova, a broj ponena koje može
nositi svaki deo se upisuje ispod odgovarajuće oznake (ispod *Deo 1* do
*Deo 5*). Ukoliko delovi nisu potrebni, može se u prvi upisati broj
poena, a u ostale se može upisati 0. Nazivi *Deo 1* do *Deo 5* se mogu
promeniti, ukoliko je potrebno. Unos ocene o pojedinim delovima se vrši
u procentima (0-100). Ukoliko je neko polje za procenat poena prazno, a
trebalo bi da je popunjeno, njegova pozadina će biti crvena (indikacija
da neki deo zadatka još nije pregledan). Polje *Komentar* služi za
upisivanje kometara o zadatku (šta nije valjalo, na primer). Ukoliko
ovakav unos nije zadovoljavajući, može se pobrisati sve sem prvih 5
kolona, s tim da se **mora obezbediti** da se u šestoj koloni (*Poeni*,
E) nalazi broj poena osvojen na proveri.

*ListBox* sa brojevima indeksa u gornjem levom uglu je namenjen
studentskom uvidu u radove. Kada se iz liste odabere neki indeks, biće
prikazani samo ti podaci, dok će ostali biti sakriveni. Ukoliko se
odabere vrednost “svi”, biće prikazani podaci o svim studentima.

Ukoliko su osnovni podaci o studentima (radno mesto, indeks, ime i
prezime) ručno uneseni, ili je nešto kod njih menjano (prvenstveno se
odnosi na kolonu sa brojevima indeksa), treba pritisnuti dugme
*Popuni listu*, čime će se nanovo izgenerisati podaci za listu indeksa i
odraditi korekcija zapisa indeksa da udu u skladu sa nalozima u
laboratorijama. Ova lista se, inače, čuva u koloni Y u tabeli
*RasporedMI.*

### Tabele RasporedMI/JUG

Sadržaj ovih tabela se automatski generiše na osnovu podataka iz tabele
*Pregledanje.* Služi za štampanje rasporeda sedenja na testu (sve je već
definisano, samo treba odabrati *Print*, odnosno *Export to PDF*,
ukoliko direktno štampanje nije moguće). U koloni Y se nalaze podaci za
*ListBox* iz tabele *Pregledanje* (ovo se ne štampa).

### Tabela Statistika

U ovoj tabeli se nalazi lokalna statistika za datu grupu i omogućuje se
korekcija rezultata na nivou grupe.

U gornjem levom uglu se nalazi tabela koja omogućava prikaz statistike
raspodele poena po opsezima. U neosenčena polja treba upisati opsege
poena koji su potrebni, a prikaz se može videti na grafikonu *Procenti*.

Ispod ove tabele se nalazi odeljak za korekciju poena na nivou grupe. U
polje *Željeni broj poena* treba upisati šta se očekuje kao prihvatljiv
broj poena za jednog studenta (na primer, za test koji nosi 20 poena,
ukoliko je prihvatljivo 75% osvojenih poena, ovde treba upisati 15). U
poljima ispod ovoga će se ispisati koliko studenata ima željeni ili veći
broj poena, kao i koji je procenat takvih studenata u grupi.

Korekcija broja poena na nivou grupe je realizovana korišćenjem funkcije
normalne raspodele čiji se parametri mogu kontrolisati sa 3 klizača i
čiji se prikaz nalazi direktno ispod ove tabele. Na broj poena studenta
se dodaje vrednost funkcije pomnožena sa njegovim brojem poena, uz
poštovanje maksimalnog broja poena na testu. Sva tri klizača svoju
vrenost izražavaju u procentima. Klizač *Sredina* se odnosi na tačku
koja predstavlja maksimum krive (na primer, ukoliko je maksimalan broj
poena na testu 20, vrednost 50% će označavati 10 poena). Klizač *Širina*
određuje širinu krive, dok klizač *Visina* određuje njenu visinu.
**Ukoliko se vrednost *****Visina***** nalazi na 0%, nema nikakve
korekcije**. Promena parametara krive se automatski odražava na osvojene
poene, što se može direktno očitati na grafikonima. Radi preciznije
kontrole parametara, može se posmatrati i grafikon *Korekcija poena*
koji prikazuje koliko poena se dodaje (crvena boja) za svaki mogući broj
poena pojedinačno (plava boja). Kod ovog grafikona treba zanemariti
poene koji su veći od maksimalnog broja poena, pošto za njih korekcije
nisu ispravno prikazane.

Evidencija.ods
--------------

Namena ovog fajla je čuvanje svih podataka o studentima iz jedne godine.
Na svim iole bitnim mestima u fajlu se nalazi pomoć u vidu oblačića sa
objašnjenjem čemu to polje/kolona služi (polja sa crvenim kvadratićem u
gornjem desnom uglu). Ukoliko neke kolone nisu od značaja (npr. ukoliko
se ne radi svih 5 testova), mogu se po volji sakriti (desni klik na
kolonu, pa *Hide*), ali ih **ne treba brisati**. **Sve kolone čiji je
sadržaj zasenčen nisu predviđene da se ručno menjaju**. **Nazivi kolona
koji su ispisani crvenom bojom nisu predviđeni da se menjaju**. Takođe,
**bitno je da se u svim kolonama sa poenima nalaze crtice na mestima
koja bi trebalo da su prazna** (može se odraditi pomoću tabele *Alati*,
pa *Unos rezultata/Obriši rezultate*).

### Tabela Studenti

U ovoj tabeli se nalaze svi podaci o svim studentima sa godine. U
odeljku *Opšti podaci* se nalaze osnovni podaci o studentima. Ove
podatke treba uneti na početku godine, i **sortirati ih po broju
indeksa**. Studenti će se u generisanim spiskovima pojaviti u redosledu
u kome su u tabeli Studenti. Red za jednog studenta se može dobiti
klikom na dugme *Dodaj red*. Ukoliko se u tabeli nalaze studenti sa više
od jednog smera, kolona *Smer* treba da sadrži oznaku smera na kom se
student nalazi (npr. RA, ESI, SIT i slično). U kolonu *Grupa*
(grupa kojoj student pripada, onako kako definiše Studentska služba) se
može uneti proizvoljna oznake grupe (na primer 1,2,...,A,B,...) u kome
student pohađa vežbe. Kolona *Grupa* ima jednu specijalnu predefinisanu
oznaku za studente koji nisu regularno na vežbama, a to je “**N**”.
Oznaka se može koristiti za studente koji dolaze na vežbe, ali nisu na
zvaničnim spiskovima studentske službe, pa stoga ne mogu dobiti potpis,
niti im može biti upisana ocena (verovatno nije regularno ni objaviti
njihove rezultate testova, no nije 100% provereno). U kolonu *Termin* se
unosi numerička oznaka termina (na primer 1,2,3,...) u kome student
pohađa vežbe (termini bi trebalo da su redom numerisani, u rastućem
redosledu dešavanja u toku radne nedelje, ili po nekom drugom sistemu).
Popunjavanje kolone je neophodno za generisanje predefinisanog izveštaja
*Spiskovi studenata po terminima*. Popunjavanje kolona *Grupa* i
*Termin* može biti korisno ukoliko treba kreirati statistiku po
grupama/terminima (na primer, koristeći *GFilter*, odnosno *Tfilter*). U
kolonu *Vezana Ocena* se može upisati ocena iz nekog predmeta koji je
bitan za tekući predmet (na primer, za Arhitekturu računara, to je ocena
iz C‑a). Ovo takođe može biti interesantno za statistiku.

**NAPOMENA**: radi razumevanja značenja kolona u ovom odeljku, neophodno
je biti upoznat sa dokumentom “Pravila polaganja AR/OS/PP” u kome je
objašnjen način ocenjivanja.

Odeljak ***Predispitne obaveze*** se odnosi na testove koji pripadaju
predispitnim obavezama. Predviđeno je ukupno do 5 redovnih testova,
nadoknada i kućni test. Kolone *Test1* do *Test5* čine redovne testove
koji se drže u toku nastave i predviđeni su planom i programom. Kolona
*PredispR* sadrži poene sa redovnih testova, bez bonus poena. Kolone
*T1Pon* do *T5Pon* su pomoćne kolone, koje služe za uključivanje poena
sa nadoknade, dok je *PredispD* kolona koja sadrži predispitne poene
uključujući nadoknadu. Kolona *PravoN* sadrži redni broj predispitne
obaveze ako student ima pravo na njenu nadoknadu, dok kolona *Nadoknada*
sadrži poene osvojene na nadoknadi. Kolona *ForsNad* služi da se nekom
studentu forsirano dozvoli izlazak na nadoknadu, iako nema prava na nju
i tu treba upisati redni broj predispitne obaveze koja se nadoknađuje
(pri čemo ovo ne važi za studente koji su uhvaćeni u prepisivanju).
Kolona *PravoK* sadrži informaciju o tome da li student imapravo da radi
kućni test, kolona *Kućni* sadrži broj poena osvojenih na kućnom testu,
a kolona *KućniD* sadrži broj poena za kućni test koji se računaju (samo
dopuna do minimalnog broja poena za izlazak na ispit). Kolone
*Nadoknada* i *Kućni* se mogu automatski popuniti (tabela *Alati*), a
mogu se unositi i ručno. Kolona *PredispBB* (predispitne bez bonusa)
sadrži broj poena koje je student sakupio na predispitnim obavezama, bez
dodatnih (bonus) poena. Kolonama se mogu dati proizvoljna
imena, ukoliko je potrebno, **osim kolona čija su imena
crvenom bojom**.

Odeljak ***Ispitne obaveze*** se odnosi na testove koji pripadaju
ispitnim obavezama. Predviđeno je ukupno do 4 parcijalna testa i do 8
ispitnih testova (odnosno ispita koji se drže u redovnim ispitnim
rokovima). Kolone *ParcI1* do *ParcI4* su rezervsane za
parcijalne ispite koji se održavaju u toku nastave. Kolone
*Ispit1* do *Ispit8* su namenjene ispitima koji se drže u
ispitnim rokovima; njihov sadržaj kumulativno dopunjuje poene za
predispitne obaveze ukoliko student nije položio ispit (do maksimalnih
55 poena), odnosno zamenjuje broj poena na parcijalnim ispitima ukoliko
je student poništio ranije ispite. Poništavanje parcijalnih testova,
odnosno ispita se može odraditi eksplicitno upisivanjem rednog broja
ispita (za parcijalne ispite redni broj je 0) u kolonu *PonisI*, dok se
može raditi i automatski, gde se kao broj poena sa ispitnih obaveza
uzimaju poeni sa poslednjeg ispita (za ovo služe kolone *PoslIspit*,
*SumaParc* i *SumaIspit*). Kolone *Parc1* do *Parc4* i *Ispit1* do
*Ispit8* se mogu automatski popuniti pomoću tabele *Alati*, a mogu se
unositi i ručno. Kolona *IspitneBB* (ispitne bez bonusa) sadrži broj
poena koje je student sakupio na ispitnim obavezama (bez bonus poena).
Kolonama se mogu dati proizvoljna imena (prethodno treba pogledati tekst
o predefinisanim izveštajima), ukoliko je potrebno, **osim kolona čija
su imena crvenom bojom**.

Odeljak ***Aktivnost*** sadrži podatke o aktivnosti studenata. Tu se
beleže podaci o broju dobijenih pluseva i minusa na vežbama (kolone
*Plus* i *Minus*), podatak o detektovanom prepisivanju (*Prepis*),
podatak da na nekog treba obratiti pažnju na testu (*Pažnja*),
procentualno izražena prisutnost na vežbama (kolona *PrisutV*), kao i
broj evidentiranih prisustvovanja predavanjima (kolona *PrisutP*).
Podaci o plusevima i minusima se mogu automatski uvući iz CSV izlaza
Plus/Minus programa. Ukoliko je bilo šta upisano u kolonu *Prepis*,
student neće biti uključen u spiskove za dodatne testove i nadoknade.
Kolona *Pažnja* se može ubaciti u spiskove po grupama, i služi da
obeleži "nemirne" studente koje treba rasporediti u prve redove (ukoliko
je student uhvaćen u prepisivanju, kolona se automatski popunjava
sadržajem istim kao kolona *Prepis*). Kolone *Do Potpisa*, *Do Ocene* i
*Do Više Ocene* služe za lakše uočavanje studenata kojima bonus poeni
mogu pomoći (sadrže odgovarajući broj poena koji nedostaju studentu).
Kriterijum ispisa poena u ovim kolonama se zadaje na stranici *Alati*
(inicijalno je postavljeno da će broj poena biti prikazan ako studentu
nedostaje do 3 poena za odgovarajuću kategoriju). Kolona *BonusV* sadrži
poene koji se dodaju studentima za aktivnost na vežbama (ovo su poeni
koji se dodaju na predispitne obaveze), pri čemu to može biti ili
prekopiran broj pluseva ili nešto drugačije računanje za Programske
prevodioce (pogledati *Podešavanja* u tabeli *Alati*)*. *U kolonu
*BonusP* se upisuju poeni koji se daju studentima za aktivnost na
predavanjima (ovi poeni se dodaju na ispitne obaveze). Kolone *KorekV* i
*KorekP* služe za ručnu korekciju poena za predispitne, odnosno ispitne
obaveze (kako bi bonus poeni ostali samo za studente koji su se
istakli).

Odeljak ***Finalno*** sadrži više kolona koje se automatski popunjavaju.
Kolone *PredispK* i *IspitneK* sadrže predispitne i ispitne poene
sabrane sa *KorekV*, odnosno *KorekP*. Kolona *Hoće6* je namenjena za
slučaj kada student ima dovoljno predispitnih poena da se ne kvalifikuje
za dopunjavanje, ne može da sakupi minimalan broj ispitnih poena na
ispitu, a ima ih u sumi dovoljno za ocenu. Tada se, na zahtev studenta,
poeni mogu preraspodeliti tako da se zadovolje minimumi za ispitne i
predispitne obaveze, a da u sumi bude 55 poena, i u tom slučaju se
preraspodeljeni poeni nalaze u kolonama *Predisp6* i *Ispitne6*. Kolona
*Predispitne* sadrži broj poena koje je student sakupio na predispitnim
obavezama sabranim sa brojem poena u kolonoi *BonusV*. Kolona *Ispitne*
sadrži broj poena koje je student sakupio na ispitnim obavezama sabranim
sa brojem poena u kolonoi *BonusP*. Da li se bonus poeni mogu dodati ili
ne se određuje u koloni *BonusOK*. U koloni *Ispit* se nalazi podatak da
li student ima pravo da izađe na ispit. Kolona *Poeni* sadrži ukupan
broj poena koje je student osvojio, pri čemu se poeni sa predispitnih i
ispitnih obaveza zasebno zaokružuju. Kolona *Ocena* sadrži ocenu, s tim
da se za ocene manje od 6 ispisuje crtica.

U dokumentu se nalaze dve predefinisane kolone za filtriranje -
*TFilter*, koja omogućava filtriranje studenata iz zadatog termina, i
*GFilter*, koja omogućava filtriranje studenata iz zadate grupe (željeni
termin, odnosno grupa se unose u polje iznad naziva kolone). Ovakvi
filteri se, na primer, mogu koristiti prilikom generisanja izveštaja,
kako bi se izdvojili studenti po grupama ili terminima.

Kolone *StatPoeni*, *StatProc* i *OdDoFilter* se koriste za tabele za
statistiku. Iza ovih kolona se mogu ubaciti proizvoljne kolone, ukoliko
su potrebne.

### Tabela CSV

Ovo je pomoćna tabela koja se koriti prilikom automatskog ubacivanja
rezultata testova/provera.

### Tabela Poeni

U ovoj tabeli se nalazi statistički prikaz poena koje su studenti
ostvarili na odabranom testu. Naziv traženog testa treba odabrati iz
liste ponuđenih (lista je identična onoj na tabeli *Alati*).
Takođe, smer treba odabrati iz liste ponuđenih. Predviđeno
je da se na jednom testu ne može dobiti više od 50 poena. Ukoliko se
sadržaj grafika ne osveži nakon izbora testa, pritisnuti F9.

### Tabela Procenti

U ovoj tabeli se prikazuju statistički podaci o bilo kojoj koloni iz
tabele *Studenti* sa proizvoljnim opsezima (do 8 proizvoljnih opsega
plus dva unapred određena). Naziv kolone se upisuje u polje *A2.* Smer
treba odabrati iz liste ponuđenih. U tabelu koja počinje od četvrtog
reda treba uneti opsege koji se posmatraju. Treba popunjavati samo polja
koja **nisu osenčena**. U redu 5 se računa koliko studenata se uopšte
nije pojavilo na testu (ukoliko nisu radili neki test, tada za broj
poena kod takvih studenata stoji “-”). Red 6 služi da objedini studente
koji se uopšte nisu pojavili sa onima koji imaju neki minimalan broj
poena (u ovom redu se može menjati samo polje *B6*). Polja od *A7* do
*B14* se popunjavaju proizvoljnim opsezima vrednosti. U koloni
*Studenata* se nalazi ukupan broj studenata koji imaju broj poena iz
zadatog opsega. Opcija *Samo sa poenima* omogućava da se kod računanja
procenata ne uzimaju o obzir studenti koji nisu izašli na odabrani test.
Ukoliko se sadržaj grafika ne osveži nakon unošenja naziva kolone,
pritisnuti F9.

Ukoliko je potrebno napraviti spisak studenata koji upadaju u neki opseg
poena, to se može ostvariti popunjavanjem opsega u odeljku
*Spisak studenata za opseg poena* i klikom na dugme *Spisak Od‑Do*.

### Tabela Alati

U ovoj tabeli se nalaze alati za automatizaciju rada sa podacima o
studentima.

#### Unos rezultata

Ovde se nalaze komande za rad sa rezultatima testova rađenih kako preko
softvera za testiranje (*Otisak*, *ZIP* fajlovi), tako i testova koji se
ručno pregledaju (*Pregled.ods* fajlovi). Da bi ovo ispravno radilo,
potrebno je obeležiti testove i provere koje će studenti raditi tako što
će se u tabeli *Studenti* za nazive tih testova staviti neka (bilo koja)
boja pozadine (bitno je samo da nije *Automatic*, odnosno *No Fill*).
Nakon toga, treba kliknuti na dugme *Ubaci Nazive*, što će popuniti
spisak naziva i listu sa tim nazivima (takođe, ponovo će se popuniti i
lista na tabeli *Poeni*). Kada su nazivi popunjeni, na raspolaganju su
sledeće komande: *Odaberi* (putanju do direktorijuma sa rezultatima
testa), *Ubaci Rezultate* i *Obriši Rezultate*.

Dugme *Odaberi* vrši odabir direktorijuma u kome bi trebalo da se nalaze
svi *ZIP* fajlovi sa jednog *Otisak* testa, odnosno svi *ODS* fajlovi sa
jednog ručno pregledanog testa. Ova putanja se može i ručno uneti u
polje ispod (*B4*).

Dugme *Ubaci Rezultate* služi da se u spisak ubace podaci o testu (koji
je izabran iz liste *Naziv*). Ovime se otvara dijalog gde se mogu videti
podaci o putanji i o testu/proveri u koji će se ubacivati rezultati, kao
i dva dugmeta, *Ubaci Rezultate* i *Izlaz*. Dugme *Ubaci Rezultate*
pokreće ubacivanje rezultata u spisak. Prilikom ubacivanja, u donjem
delu dijaloga će biti ispisani rezultati ubacivanja, kao i sve
nepravilnosti na koje se naišlo prilikom ubacivanja:

-   ukoliko je neki student već radio test/proveru (taj će dodatno biti
    obeležen crvenom bojom u spisku)
-   ukoliko se student ne nalazi na spisku

Ovaj izveštaj će se snimiti u direktorijum u kome su *ZIP*/*ODS*
fajlovi. Dugme *Izlaz* služi za zatvaranje dijaloga. Takođe, pre
ubacivanja se može specificirati da li će se prijavljivati višestruki
izlasci na test, kako će se ubacivati negativni poeni (kao negativni ili
će se poništavati na nulu) i da li se želi da se poeni sa testa koji se
ubacuje dodaju na postojeće poene (neophodno ukoliko se ispit sastoji iz
više od jednog dela; uključenje ove opcije podrazumeva deaktiviranje
detekcije višestrukih izlazaka). Ukoliko je za nekog studenta u *ODS*
fajlu popunjeno polje *Prepis*, naziv testa će se upisati u njegovu
kolonu *Prepis* u tabeli *Studenti*.

Komanda *Obriši Rezultate* služi za brisanje podataka o testu (koji je
izabran iz liste *Naziv*). Svi poeni se postavljaju na “-”, i uklanjaju
se eventualne oznake o nepravilnostima.

#### Generisanje Pregled.ods fajla

Ovaj odeljak služi za automatsko generisanje
*Pregled\_YYYY-MM-DD\_DAN\_HH-MM-LAB-SMER.ods* fajlova za ručno
pregledanje testova. Nakon pritiska na dugme *Odaberi*, treba odabrati
*CSV/TXT* iz (raspakovane) arhive koja sadrži rezultate jednog testa
(onako kako ga generiše automatsko sakupljanje zadataka). Nakon odabira,
biće kreiran novi *Pregled.ods* fajl u istom direktorijumu kao i
*Evidencija.ods*, s tim što će njegovom nazivu biti dodato vreme
kreiranja *CSV/TXT* fajla, naziv učionice ukoliko je detektovan i
odabrani naziv smera iz odeljka *Izbor smera*. Izveštaj o generisanju
*Pregled* fajla će se takođe snimiti u istom direktorijumu. Ukoliko se
obeleži stavka *Podaci za nadoknadu*, pored kopiranja podataka o
indeksu, imenu i dosada osvojenim poenima sa predispitnih obaveza, u
novi *Pregled.ods* će se iskopirati i odgovarajući sadržaj kolone
*PravoN* iz tabele *Studenti*.

#### Dodatni predispitni testovi

U ovom odeljku se nalazi generisanje izveštaja vezanih za dodatne
testove. U poljima ispod naziva odeljka treba upisati koji je minimalan
broj poena sa predispitnih obaveza koje student mora imati da bi se
mogao kvalifikovati za kućni test, odnosno procenat poena na
pojedinačnim testovima da bi mogao izaći na nadoknadu. Nakon bilo kakve
izmene u ovim poljima, treba kliknuti na dugme *Osveži Podatke*, kako bi
se promene odrazile na polja u tabelama.

Da bi generisanje spiska za nadoknadu moglo da se odradi, treba popuniti
i maksimalan broj poena koji se može ostvariti na predispitnim testovima
u delu Max broj poena za testove (za svih 5 redovnih predispitnih
testova).

Dva dugmeta u donjem delu omogućavaju generisanje spiskova studenata
koji imaju pravo da rade kućni test, odnosno da izađu na nadoknadu.
Ukoliko je stavka *Isključi neregularne* iz odeljka za generisanje
izveštaja uključena, u izveštaj neće ući neregularni studenti.

#### Generisanje izveštaja

Ovaj deo služi za pravljenje izveštaja vezanih za tabelu *Studenti.*
Polje *Podela po prolaznosti* služi za odabir da li će se studenti
deliti na one koji imaju ocenu, koji se pozivaju na ispit i koji nisu
stekli uslove za potpis.

Polje *Broj kolona* služi za odabir u koliko kolona će se generisati
izveštaj. Polje *Broj vrsta* određuje koliko maksimalno vrsta će se
izgenerisati pre nego se napravi razmak i ponovo ubaci zaglavlje tabele
(ukoliko spisak treba deliti na više strana).

Polje *Filter* služi za definisanje kolone koja će služiti kao filter
prilikom generisanja izveštaja (u izveštaj će ući samo oni studenti kod
kojih odgovarajuće polje u ovoj koloni nije prazno, pri čemu se i crtica
računa kao prazno; može biti korisno ako, na primer, treba generisati
izveštaj samo za studente koji su izašli na neki od ispita). Kao kolona
za filter se može iskoristiti bilo koja kolona iz tabele *Studenti*.
Polje *Inverzni filter* omogućava da se posmatra inverzna varijanta
uslova za filtriranje podataka. Kao primer posebnog filtera, može se
pogledati polje *TFilter*, odnosno *Gfilter*.

Polje *Isključi neregularne* služi za isključivanje neregularnih
studenata iz izveštaja. Neregularni studenti su svi oni koji za polje
*Grupa* u tabeli *Studenti* imaju oznaku “**N**”. Takođe, ukoliko je ova
opcija uključena, takvim studentima se neće popunjavati poeni prilikom
generisanja fakultetskog zapisnika.

U delu *Kolone* treba upisati nazive kolona iz drugog reda tabele
*Studenti* koje su potrebne da se nađu u izveštaju. Kolone će se u
izveštaju naći u redosledu u kojem su ovde navedene, pri čemu se prazna
polja preskaču.

U delu *Izbor smera* treba upisati nazive svih smerova koji se
pojavljuju u koloni *Smer* tabele *Studenti*. Nakon popunjavanja
smerova, treba kliknuti na dugme *Ubaci Smerove*, čime će se popuniti
lista za izbor smera. Za studente koji ponovo slušaju predmet, ukoliko
ih je potrebno izdvojiti, može se napraviti poseban "smer" za njih, npr
PON. Na taj način je moguće zasebno videti statistike i spiskove za
redovne studente i za one koji ponovo slušaju i/ili polažu premet.

U delu *Naziv izveštaja* se unosi ime koje će se nalaziti u zaglavlju
izveštaja.

Konačno, dugme *Generiši Izveštaj* će na osnovu podataka upisanih u ovom
delu izgenerisati izveštaj u tabeli *Izveštaj.* Pošto generisanje baš i
nije naročito optimizovano (a velika je šansa da neće ni biti), treba
sačekati dok se na ekranu ne pojavi obaveštenje da je izveštaj
izgenerisan. Nakon generisanja izveštaja, on će biti automatski
formatiran i biće snimljena njegova *PDF* verzija u istom direktorijumu
u kome je i *Evidencija.ods*.

#### Podešavanja

Ovde se nalaze parametri koji utiču na generisanje ocena*.* Prvo polje
određuje minimalan broj poena potrebnih da studentu bude dat potpis iz
predmeta. Drugo polje određuje minimalan broj poena potrebnih da
studentu dobije ocenu. Sledeća tri polja određuju uslove pojavljivanja
poena u kolonama *Do Potpisa*, *Do Ocene* i *Do Više Ocene* iz odeljka
***Aktivnost*** u tabeli *Studenti.* Naredna dva polja sadrže limite za
broj poena za predispitne, odnosno ispitne obaveze. Naredno polje sadrži
minimalan broj poena na predispitnim obavezama da bi student bio pozvan
na dodatni test za sticanje poena za izlazak na ispit. Polje *PP Bonusi*
određuje da li se *BonusV* kolona u tabeli *Studenti* popunjava
vrednošću kolone *Plus*, ili se radi skaliranje po pravilima za
Programske prevodioce (broj pluseva podeljen sa 5 + 1, ali samo ukoliko
nema minusa). Poslednje polje određuje minimalan broj poena sa ispitnih
obaveza koji su potrebni za potpis. Nakon bilo kakve izmene u ovim
poljima, treba kliknuti na dugme *Osveži Podatke*, kako bi se promene
odrazile na polja u tabelama.

#### Predefinisani izveštaji

Ovaj deo služi za pravljenje predefinisanih izveštaja vezanih za tabelu
*Studenti.* Dva polja odmah ispod naslova odeljka treba popuniti nazivom
predmeta (npr. "Arhitektura računara") i školskom godinom (npr.
"2016/2017"). Ukoliko je u odeljku *Generisanje izveštaja* odabran neki
smer, tada će se predefinisani izveštaji generisati za taj smer. Ako u
polju za izbor smera stoji *Svi smerovi*, tada će se generisati
izveštaji za sve studente sa predmeta, odnosno sa svih smerova. Postoji
ukupno šest predefinisanih izveštaja:

**Sa pravom na isp.** - Spisak studenata sa pravom izlaska
na ispit. Ovaj spisak čine sledeće kolone iz tabele *Studenti*:
*Indeks*, *Prezime*, *Ime*, *Predispitne*, *Ispitne* i
*Poeni*.

**Bez prava na isp.** - Spisak studenata bez prava izlaska na ispit.
Ovaj spisak čine sledeće kolone iz tabele *Studenti*: *Indeks*,
*Prezime*, *Ime*, *Predispitne*, *Izpitne* i *Poeni*.

**Sa Ocenom** - Spisak studenata sa
prolaznom ocenom. Ovaj spisak čine sledeće kolone iz tabele *Studenti*:
*Indeks*, *Prezime*, *Ime*, *Predispitne*, *Izpitne*, *Poeni* i
*Ocena*.

**Za Ispit** - Spisak studenata za ispit.
Ovaj spisak čine sledeće kolone iz tabele *Studenti*: *Indeks*,
*Prezime*, *Ime*, *Predispitne*, *Izpitne* i *Poeni*, dopunjene praznom
kolonam *Ocena*. Spisak čine svi studenti koji imaju pravo na potpis,
ali nemaju prolaznu ocenu.

**Poeni za test** - Osvojeni poeni na odabranom testu.
Ovaj spisak čine sledeće kolone iz tabele *Studenti*: *Indeks*,
*Prezime*, *Ime* i test čiji je naziv odabran u odeljku
*Unos rezultata*. Spisak čine samo oni studenti koji su izašli na
odabrani test.

**Poeni test + ukup.** - Osvojeni poeni na odabranom testu. Ovaj spisak
čine sledeće kolone iz tabele *Studenti*: *Indeks*, *Prezime*, *Ime*,
test čiji je naziv odabran u odeljku *Unos rezultata*, *Poeni* i
*OcenaC*. Spisak čine samo oni studenti koji su izašli na odabrani test.

**Spiskovi po term.** - Spisakovi studenata po terminima. Da bi ovaj
izveštaj mogao da se generiše, tabela *Nazivi termina po smerovima* mora
biti ispravno popunjena i polje *Termin* mora biti popunjeno za sve
studente u tabeli *Studenti*. Ukoliko na predmetu postoji više od jednog
smera, prethodno se smer mora odabrati u odeljku
*Generisanje izveštaja*. Za svaki termin će se izgenerisati posaban PDF
fajl. Za spiskove po terminima se može odabrati da li će se u spisak
uključiti i kolona sa podacima o prepisivanju, ako se odabere polje
*Prepis kolona*.

**Prva 4 spiska** - Generiše prva 4 predefinisana izveštaja: sa i bez
prava izlaska na ispit, sa i bez ocene.

Za svaki izveštaj postoji dugme koje aktivira njegovo generisanje.
Rezultat generisanja izveštaja je formatiran spisak studenata u posebnoj
tabeli i *PDF* fajl sa tim istim spiskom snimljen na istu lokaciju kao i
*Evidencija.ods*. Prilikom generisanja izveštaja, biće modifikovana
polja odeljka *Generisanje izveštaja*, pošto se ona interno koriste.

Nazivi izveštaja se po potrebi mogu menjati unošenjem drugih naziva u
odgovarajuća polja pored svakog spiska.

Generisanje predefinisanih izveštaja je **vezano za nazive pojedinih
kolona onako kako su dati u originalnom dokumentu** (konkretno, koriste
se nazivi *Indeks*, *Prezime*, *Ime*, *Predispitne*, *Ispitne*, *Poeni*
i *Ocena*). Ovi izveštaji su vezani i za dodatno definisani stil strane
(*Page Style*) nazvan "Spisak". Ukoliko se ovaj stil menja, može se
desiti da generisanje predefinisanih izveštaja ne radi kako treba.
Prilikom generisanja izveštaja se brišu svi definisani *Print Range*-ovi
i menja se stil strane "Spisak" (ovo treba imati na umu ukoliko se
izveštaji generišu i ručno i automatski).

Dugme *Obriši izveštaje* briše sve izgenerisane predefinisane izveštaje
(tačnije, sve tabele čija imena počinju sa "Spisak" – **obratiti
pažnju!**).

Ukoliko je stavka *Isključi neregularne* iz odeljka za generisanje
izveštaja uključena, u izveštaj neće ući neregularni studenti.

#### Popunjavanje fakultetskog zapisnika

Ovaj deo služi za automatsko popunjavanje fakultetskih zapisnika koji se
od Studentske službe dobijaju u *XLS* formatu, odnosno za generisanje
CSV fajlova za uvoz u Elektronsko ocenjivanje na FTN Nastavničkom
servisu (spisak studenata se kao PDF fajl može preuzeti sa Elektronskog
ocenjivanja, u gornjem levom uglu odeljka Rezultati). Dugme *Odaberi*
služi za izbor *XLS* fajla zapisnika, odnosno PDF fajla sa spiskom
studenata prijavljenih za ispit. Putanja do ovog fajla se nakon toga
upisuje u polje ispod (može se uneti i ručno). Dugme *Ubaci podatke*
vrši ubacivanje podataka u XLS fajl (nakon ubacivanja podataka, fajl
ostaje otvoren, a korisnik ga treba sam snimiti), odnosno generisanje
CSV fajla sa rezultatima ukoliko je odabran PDF fajl sa spiskom
studenata.

Ukoliko je uključena opcija *Isključi neregularne* u odeljku
*Generisanje izveštaja*, ocena se neće popuniti za studente koji nisu
regularno na predmetu.

**NAPOMENA**: Funkcionalnost vezana za generisanje CSV zapisnika za
Nastavnički servis se zasniva na **poppler-utils** paketu na
Debian-baziranim distribucijama (tačnije, koristi **pdftotext** iz tog
paketa) i na nekoliko drugih *Linux* alata (**bash**, **tr**, **grep**,
**tail** i **gawk - GNU Awk - BITNO!!!**).

#### Unos aktivnosti studenata

Ovaj deo služi za ubacivanje podataka o aktivnosti studenata. Ovi podaci
mogu doći iz 3 izvora:

-   iz CSV izlaza ***A****uthlog****A****nalyser**** ***programa, odakle
    se dobijaju podaci o prisutnosti na vežbama
-   iz CSV izlaza ***PlusMinus*** programa, odakle se dobijaju podaci o
    plusevima i minusima na vežbama
-   iz ODS fajla sa bonus poenima sa predavanja (*BonusPredavanja.ods*)

Dugme *Odaberi* služi za izbor fajla u kome se nalaze
podaci o aktivnosti. Putanja do ovog fajla se nakon toga upisuje u polje
ispod (može se uneti i ručno). Dugme *Ubaci podatke* vrši ubacivanje
podataka iz zadatog fajla, a dugme *Obriši podatke* briše sve podatke o
aktivnosti.

NAPOMENA: prilikom uvoza, podaci o plusevima, minusima i bonus poenima
će se DODAVATI na postojeće. Ovo je urađeno kako bi se korektno obradili
podaci sa više izvora (npr. ako studenti imaju vežbe i u MI i u JUG
laboratorijama, pošto se vode posebne evidencije na ta dva mesta).

\[verovatno će bti izbačeno u narednoj verziji\] Ukoliko se detektuje da
se neki student pojavio više od jednom na vežbama u toku jedne nedelje
(to radi program za analizu logova), polje *BonusV* za datog studenta će
se obojiti u crveno. Ovakve slučajeve treba posebno proveriti,
korišćenjem programa za analizu logova. Ako ovaj podatak nije od
značaja, treba ručno postaviti boju pozadine na tim poljima na
*No Fill* odnosno *Automatic*.

#### Uvoz podataka o studentima

Ovaj odeljak omogućava automatsko popunjavanje/dopunjavanje spiska
studenata iz XLS fajlova generisanih iz FTN Nastavničkog servisa ili
dobijenih od Studentske službe (po grupama ili za polaganje ispita).
Dugme *Odaberi* služi za izbor *XLS* fajla u kome se nalazi spisak
studenata. Putanja do ovog fajla se nakon toga upisuje u polje ispod
(može se uneti i ručno). Generalno, prvo treba ubaciti spiskove po
grupama i smerovima (odeljak "Brojna stanja" sa nastavničkog servisa, pa
"Grupe na nivou semestra", ili spiskovi po grupama dobijeni od
studentske službe), a onda dodati studente sa spiska za polaganje
(odeljak "Polaganje ispita" sa nastavničkog servisa).

Pre samog ubacivanja, treba odabrati smer u odeljku *Izbor smera*. Dugme
*Ubaci podatke* vrši ubacivanje podataka iz zadatog fajla, pri čemu se
dodaju samo studenti koji već nisu u Evidenciji.

Za spisak po grupama, kao smer se stavlja odabrani smer, a grupa se
određuje automatski. Za spisak za polaganje, smer se određuje na osnovu
načina polaganja i prva dva slova indeksa, dok grupu treba uneti ručno.
Za studente koji predmet ponovo polažu preko kolokvijuma (odeljak
"Polaganje ispita" sa nastavničkog servisa, način polaganja "Polaže
preko kolokvijuma") za smer će se staviti "PON". Za studente za koje se
ustanovi da prvi put slušaju predmet ("Prvi put sluša") ili su uplatili
za kompletno ponovno slušanje ("Ponovo sluša") za smer će se upisati:

-   "RA" ako im indeks počinje se "RA"
-   "PSI" ako im indeks počinjue sa "E3"
-   prva dva znaka indeksa praćena znakom pitanja u ostalim slučajevima
    (za ove studente smer treba ručno odrediti)

Izveštaj o ubačenim studentima će se snimiti u fajl u istom
direktorijumu gde je i Evidencija.

#### Nazivi termina po smerovima

U ovoj tabeli se nalaze nazivi termina koji se koriste za generisanje
spiskova po terminima. Za svaki smer treba uneti naziv termina u
odgovarajući red. Prilokom generisanja spiskova po terminima, ovaj naziv
će se nalaziti u zaglavlju spiska, a takođe će se iskoristiti i za naziv
fajla.

### Tabela Izveštaj

Ovde se nalazi izveštaj generisan pomoću opcija za generisanje izveštaja
iz tabele *Alati.*

### Kreiranje, kopiranje i izmena tabele *Studenti*

Kada treba dodati još redova za studente, **OBAVEZNO** koristiti
kopiranje mišem (ono kada se obeleži jedan red sa ćelijama, pa se njihov
sadržaj, uz izmene, kopira u druge redove; može i sa tastaturom, ali ne
znam kako) da bi se iskopirale i modifikovale sve formule. **Brisanje
viška redova treba odraditi sa *****Shift+****Delete***** (brisati sa
kraja).** Dodavanje jednog reda se može odraditi i preko dugmeta
*Dodaj red* u gornjem levom uglu tabele *Studenti*.

Kada se prebacuju podaci iz stare u novu verziju fajla, potrebno je
prebaciti samo one kolone u koje **NISU ZASENČENE** (kolone iz odeljka
Opšti podaci, rezultati testova i ispita, podaci o aktivnosti). Najbolje
bi bilo da se za *paste* u novi dokument iskoristi opcija
*Paste Special* (*Ctrl+Shift+v*), pa da se u opcijama u prvoj koloni
isključi *Paste all*, a ostavi uključeno samo *Text* i *Numbers*.

### Ostale tabele i napomene

Pored postojećih tabela, može se dodati proizvoljan broj drugih (za
štampanje, na primer), ali bi bilo dobro da njihova imena ne počinju sa
*Spisak*, pošto se takve tabele brišu automatski pritiskom na dugme
*Obriši izveštaje* na tabeli *Alati*. Bitno je i da se nazivi ovde
opisanih tabela ne menjaju, kao i da se ne menjaju postojeći nazivi u
tabelama, onako kako je naglašeno u opisu svake od tabela. Takođe, ne
treba menjati ni raspored, odnosno položaj kolona u tabelama, pošto su
neki proračuni vezani za tačno određene ćelije, odnosno kolone (npr, sve
u tabeli *Alati* je vezano za tačno određene ćelije). Ako su potrebne,
nove kolone treba dodavati iza, odnosno ispod postojećih.

